function output_4D = plot_coords(par, m, stax, stay)

modx = par.modx;    mody = par.mody;    modz = par.modz;
nx = length( par.modx );
ny = length( par.mody );
nz = length( par.modz );
nmod = nx*ny*nz;                                

val = zeros(nmod,1);
x_c = zeros(nmod,1);
y_c = zeros(nmod,1);
z_c = zeros(nmod,1);

n=1;
for kk = 1:nz
    for jj = 1:nx
        for ii = 1:ny
           n_indx = fast_sub2ind3d( [ny nx nz], ii, jj, kk );
           val(n) = -100*m(n_indx);
           xx     = modx(jj);
           yy     = mody(ii);
           zz     = modz(kk);
           d_o    = sqrt( xx^2 + yy^2 );
           d_c    = d_o*(6371-zz)/6371;
           r_z    = 6371 - zz;
           z_c(n) = 6371 - sqrt(r_z^2 - d_c^2);
           x_c(n) = xx*(6371-zz)/6371;
           y_c(n) = yy*(6371-zz)/6371;
           n=n+1;
        end
    end
end

dx = 10; dy = 10; dz = 20;
xi = modx(1)-2:dx:modx(end)+20;  
yi = mody(end)+2:dy:mody(1)-20;
zi = modz(1)-1:dz:modz(end)+30;
nplot = length(xi)*length(yi)*length(zi);
xg = zeros(nplot,1);
yg = zeros(nplot,1);
zg = zeros(nplot,1);
n=1;
for ii = 1:length(yi)
    for jj = 1:length(xi)
        for kk = 1:length(zi)
            xg(n) = xi(jj);
            yg(n) = yi(ii);
            zg(n) = zi(kk);
            n=n+1;
        end
    end
end
%

ind_x1  = find( xg>=max(modx) );
ind_x2  = find( xg<=min(modx) );
%ind_y1  = find( yg>=max(mody) );
%ind_y2  = find( yg<=min(mody) );
ind_z1  = find( zg>=max(modz) );
ind_z2  = find( zg<=min(modz) );

ind_all = [ ind_x1; ind_z1; ind_x2; ind_z2 ];
ind_all = unique(ind_all);
val2 = zeros(length(ind_all),1);
keyboard
x_c = [x_c; xg(ind_all)];
y_c = [y_c; yg(ind_all)];
z_c = [z_c; zg(ind_all)];
val = [val; val2 ];

%
dx = 10; dy = 10; dz = 20;
xi = modx(1):dx:modx(end);  
yi = mody(end):dy:mody(1);
zi = modz(1):dz:modz(end);
nplot = length(xi)*length(yi)*length(zi);
xg = zeros(nplot,1);
yg = zeros(nplot,1);
zg = zeros(nplot,1);
n=1;
for ii = 1:length(yi)
    for jj = 1:length(xi)
        for kk = 1:length(zi)
            xg(n) = xi(jj);
            yg(n) = yi(ii);
            zg(n) = zi(kk);
            n=n+1;
        end
    end
end

%
keyboard
m_plot = griddata(x_c,y_c,z_c,val,xg,yg,zg);      
output_4D = reshape( m_plot, [length(xg) length(yg) length(zg)] );


X = xg; Y = yg; Z = zg; V = output_4D;

save figures/output_tomo_4D X Y Z V stax stay