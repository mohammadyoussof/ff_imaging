clc
%clear 
close all
load data;
%load block_mod_invention2.mat
load df_out1.mat;
load df_outt.mat;

load trial_from_out1.mat
d3=df_out1;
d4=df_outt;

%s_static1  = m(nmodel+1:end_s_stat);
s_static2  = vm_outt(end_s_stat-79:nmodel);
%e_static1  = m(end_s_stat+1:end);
e_static2  = vm_outt(end-279:end);
dc = calc_dc(s_static2,e_static2,d3(:),ray,stn); 
%vr    = variance_reduction( d1, dc(:) );

resid2 = norm( dc(:) - d3 );



stn.resid2 = zeros(length(stn.num),1);
for ii = 1:length(stn.num)
    indx2 = find(ray.sta_num == stn.num(ii));
    stn.resid2(ii) = sum( dc(indx2)-d3(indx2) )/length(indx2); %# TRIALS
    %stn.resid(ii) = sum( (dc(indx2)-d(indx2)) )/length(indx2);
end   


figure(30)
 s_residuals2=stn.resid2;
 plot_s_resids(par, data, s_residuals2); hold on; set(gca,'clim', [(-3e-04) (3e-04)]); cs = max(abs([(-3e-04) , (3e-04)]));

  figure(4);
histfit(s_residuals2(:), 10)
 %xlim([-4e-4 3e-4])
 ylim([0 17])
title('Residuals Terms')
xlabel('seconds')
%print(['histo_resd2.pdf'],'-dpdf','-fillpage','-r1000');

figure(400);

histogram(d3(:))
 %xlim([-4e-4 3e-4])
 %ylim([0 17])
title('d4 dR Terms')
xlabel('seconds')