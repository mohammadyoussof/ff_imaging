function cflat = v_cluster_flat21(Curmodel,ncategories)


% Flatten initial model
[Ni,Nj] = size(Curmodel);
cflat = Curmodel;
% ncategories = 10;
options = [NaN NaN NaN 0];

%options = [NaN NaN 0.20 20];

[centers,~] = fcm(Curmodel(:),ncategories,options); % Find center values of velocity clusters
for i=1:Ni
    for j=1:Nj
      [~,velindex] = min(abs(centers-Curmodel(i,j)));
      cflat(i,j) = centers(velindex);
    end
end