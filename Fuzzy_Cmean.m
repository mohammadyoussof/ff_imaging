%% Fuzzy C-mean Algorithm
% Author: Tan Pham - pntan.iac@gmail.com




clc
clear all
close all

%% Source
load fcmdata.dat
source_data=fcmdata
[centers,U] = fcm(source_data,20);

maxU = max(U);
index1 = find(U(1,:) == maxU);
index2 = find(U(2,:) == maxU);

figure;
plot(source_data(index1,1),source_data(index1,2),'ob')
hold on
plot(source_data(index2,1),source_data(index2,2),'or')
plot(centers(1,1),centers(1,2),'xb','MarkerSize',15,'LineWidth',3)
plot(centers(2,1),centers(2,2),'xr','MarkerSize',15,'LineWidth',3)
title('Source');
hold off

%% Parameters
% data=[1 2;2 3; 9 4; 10 1];
data=fcmdata;
k=2;
num_data=size(data,1);
num_feature=size(data,2);
p=2;

%% Main loop
weight=Create_init_weight(num_data,k);


add_zeros=zeros(num_data,1);
final_data=[data,add_zeros];

for i=1:500
    cen=Define_centroid(data,weight,k,num_feature,p);
    weight=Update_weights(weight,cen,data,p);
end

disp(weight);

% label
for i=1:num_data
    [v,idx]=max(weight(i,:));
    final_data(i,3)=idx;
end

SSE=0;
for j=1:k
    for i=1:num_data
        SSE=SSE+weight(i,j).^p*norm(cen(j,:)-data(i,:));
    end
end
disp(final_data);
L=['SSE = ',num2str(SSE)];
disp(L)

%% Plot
figure;

hold on

for i=1:size(data,1)
    
    if final_data(i,3)==1
        plot(final_data(i,1),final_data(i,2),'ob')
    else
        plot(final_data(i,1),final_data(i,2),'or')
    end
end

plot(cen(1,1),cen(1,2),'xb','MarkerSize',15,'LineWidth',3)
plot(cen(2,1),cen(2,2),'xr','MarkerSize',15,'LineWidth',3)
title('By Tan');
hold off


%% Function list

function centroid=Define_centroid(data,weight,num_cen,num_feature,p)
centroid=zeros(num_cen,num_feature);
for i=1:num_cen
    for j=1:num_feature
        SS=sum(weight(:,i).^p);
        sumMM=sum((weight(:,i).^p).*data(:,j));
        centroid(i,j)=sumMM/SS;
        
    end
end

end

function new_weight=Update_weights(init_weight,cen,data,p)
new_weight=zeros(size(init_weight));
for i=1:size(init_weight,1)
    denoSum=0;
    for j=1:size(init_weight,2)
        denoSum=denoSum+1/norm(cen(j,:)-data(i,:)).^(1/(p-1));
    end
    for j=1:size(init_weight,2)
        w=(1/norm(cen(j,:)-data(i,:))).^(1/(p-1))/denoSum;
        new_weight(i,j)=w;
    end
    
end
end


function weight=Create_init_weight(num_data,num_cen)
W=rand(num_data,num_cen);
weight=W./(sum(W'))';
end
