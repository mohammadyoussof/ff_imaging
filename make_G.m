
function [G,par,data] = make_G( par, data )
disp(' Hey, come on it is  ::::   G Matrix Fun Time ')

if ( nargin ~= 2 || ~isstruct(par) || ~isstruct(data) )
    error( 'Input must be a parameter structure.' );
end

% convert station lat/lon to cartesian
[data.stn.x,data.stn.y] = project_xy( par, data.stn.lat, data.stn.lon );

if ( par.verbose >= 3 )
    plot_stas( par, data );    
end

notify( 'Dr. Youssof, This is creating  G   Matrix', par, 1 );
[G,data] = make_matrix( par, data );

%i just uncommented that today :)
%if ( isinf(par.omega) )
%	[G,data] = make_matrix_ray( par, data );
%else
%	[G,data] = make_matrix( par, data );
%end
%}


%
%
% SUBFUNCTIONS
%
%
% PLOT_STAS
%
function plot_stas( par, data )

clf
hold on
fprintf( 'Making Station Map\n' );

stn = data.stn;


%bbox = [min([stn.lon,stn.lat]); max([stn.lon,stn.lat])];
%S = shaperead( 'usastatehi', 'BoundingBox', bbox);
bbox = [min([stn.lon,stn.lat]); max([stn.lon,stn.lat])];
V = shaperead( 'unep_reported_malaria_cases_-_total_number_per_100000_population_world_1990-2003.shp', 'BoundingBox', bbox);
for i = 1:length(V)
	[x,y] = project_xy( par, V(i).Y, V(i).X );
	plot( x, y );
end
hold on
h = plot( stn.x, stn.y, 'ko' );
set( h, 'markerfacecolor', 'r' );

maxx = max( stn.x );
maxy = max( stn.y );
minx = min( stn.x );
miny = min( stn.y );
dx = (maxx - minx) * .1;
dy = (maxy - miny) * .1;
axis( [minx-dx maxx+dx miny-dy maxy+dy] );

title( 'WALPASS Station Map' );
xlabel X;
ylabel Y;
print(['figures/MAP_new.pdf'],'-dpdf','-r500');
pause

    
    
