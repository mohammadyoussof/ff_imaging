%%%%18 October 2012% october 2013 MY%%
function ds_map = make_DS_map_synth_crt( mf, m, par, stax, stay)

cmap=colormap(redblue);
cmap=colormap(flipud(colormap));
colormap(cmap);

modx=par.modx; mody=par.mody; modz=par.modz; nx=length(modx); ny=length(mody); nz=length(modz);
plotx = modx(2):10:modx(end-1); ploty = mody(end-1):10:mody(2); plotz = modz(1:end-1);


load data
stn = data.stn;
bbox = [min([stn.lon,stn.lat]); max([stn.lon,stn.lat])];
V = shaperead( 'unep_reported_malaria_cases_-_total_number_per_100000_population_world_1990-2003.shp', 'BoundingBox', bbox);
V1 =shaperead('ANG-15_boundaries.shp','BoundingBox', bbox);
for i = 1:length(V)
	[x,y] = project_xy( par, V(i).Y, V(i).X );
	plot( x, y );
end
hold on

for i = 1:length(V1)
	[x1,y1] = project_xy( par, V1(i).Y, V1(i).X );
	plot( x1, y1 );
end
hold on
h = plot( stn.x, stn.y, 'ko' );
set( h, 'markerfacecolor', 'r' );


% set limits for plot axes
min_x = min(plotx)+60;
max_x = max(plotx)-30;
min_y = min(ploty)+30;
max_y = max(ploty)-60;


for pz = 1:length(plotz)
    %clear m_plot
    m_plot1 = zeros(length(ploty),length(plotx));
    m_plot2 = zeros(length(ploty),length(plotx));
    for py = 1:length(ploty)
        for px = 1:length(plotx)
            pl_pt = [ ploty(py), plotx(px), plotz(pz) ];
            % find model node, upper-left-top
            my_diff = abs(mody - ploty(py)); srt_mdfy=sort(my_diff); mi = find(my_diff==srt_mdfy(1),1);
            mx_diff = abs(modx - plotx(px)); srt_mdfx=sort(mx_diff); mj = find(mx_diff==srt_mdfx(1),1);
            mz_diff = abs(modz - plotz(pz)); srt_mdfz=sort(mz_diff); mk = find(mz_diff==srt_mdfz(1),1);
            dx=0.5*(abs(modx(mj+1)-modx(mj))+abs(modx(mj-1)-modx(mj))); 
            dy=0.5*(abs(mody(mi)-mody(mi+1))+abs(mody(mi-1)-mody(mi)));     dh=sqrt(dx^2+dy^2);
            if mk>1
                dz=0.5*((modz(mk+1)-modz(mk))+(modz(mk)-modz(mk-1)));  
            elseif mk==1
                dz=modz(mk+1)-modz(mk);
            end
            dist = zeros(1,9);   val1 = zeros(1,9); val2 = zeros(1,9);
            for ii = 1:3
                for jj = 1:3
                    %for kk = 1:2
                        mod_ptx = modx(mj+jj-2);
                        mod_pty = mody(mi+ii-2);
                        mod_ptz = modz(mk);
                        mod_pt = [ mod_pty mod_ptx mod_ptz ];
                        dist(jj+(ii-1)*3) = dh - sqrt( (mod_pt(1)-pl_pt(1))^2 + (mod_pt(2)-pl_pt(2))^2 );
                        if dist(jj+(ii-1)*3)<0
                            dist(jj+(ii-1)*3) = 0;
                        end
                        mi_ind = mi+ii-2; mj_ind = mj+jj-2; mk_ind = mk;
                        mod_indx = fast_sub2ind3d( [ny nx nz], mi_ind, mj_ind, mk_ind );
                        val1(jj+(ii-1)*3) = mf(mod_indx);
                        val2(jj+(ii-1)*3) = m(mod_indx);
                    %end
                end
            end
                wt = dist./sum(dist); 
                val1 = wt.*val1;
                val2 = wt.*val2;
                plot_val1 = -100*(sum(val1)/sum(wt));
                plot_val2 = -100*(sum(val2)/sum(wt));
                m_plot1(py,px) = plot_val1;
                m_plot2(py,px) = plot_val2;
        end
    end
    
    % plot 1, output
    subplot(1,2,1);
    %hold off
    hold on
    imagesc(plotx,-ploty,m_plot1)
    shading interp
    
    
    hold on
    plot( stax, stay, 'ko','MarkerFaceColor','k','MarkerSize', 3 ); 
     plot( x, y, 'k' );
    
    
    title( sprintf('Synthetic Input, Depth = %d\n',round(plotz(pz))) );
    colorbar
    caxis( [-0.8 0.8] );
    %view(0, 90)
    axis( [min_x max_x min_y max_y] );
    
     cs = max(abs([min(m_plot1(:)),max(m_plot1(:))]));
     caxis([-cs cs])
    set(gcf,'units',get(gcf,'PaperUnits'),'Position',get(gcf,'PaperPosition'))
    
    daspect([1 1 1])
    hold on
    
%     plot( Vx, -Vy, 'k' );

    
    %today@%  plot( stax, -stay, 'ko','MarkerFaceColor','k','MarkerSize', 2 ); 
%     plot( Vx, -Vy, 'k' ); 
    
    xt=[-922 -700 -478 -256 -34 188 410 632 854]; yt= [-772 -550 -328 -106 116 338 560 782];
    xt=[-922 -700 -478 -256 -34 188 410 632 854]; yt= [-772 -550 -328 -106 116 338 560 782];
    set(gca,'XTick',xt,'YTick',yt) 
    for gg=1:length(xt)
    [ytl,xtl] = project_xy( par, xt(gg)*ones(size(yt)), -yt, 'inverse' ); 
    xla(gg)=mean(xtl);
    end

    set(gca,'XTickLabel',roundn(xla,0.0),'YTickLabel',roundn(sort(ytl, 'descend'),0.0) )
    xlabel( 'Longitude ' );
    ylabel( 'Latitude ' );
    
    % plot 2, output
	subplot(1,2,2);
    hold on
    
    imagesc(plotx,-ploty,m_plot2)
    shading interp
    title( sprintf('Synthetic Result, Depth = %d\n',round(plotz(pz))) );
    colorbar
    caxis( [-.8 0.8] );
    
    
    
    axis( [min_x max_x min_y max_y] );
    daspect([1 1 1])
    %view(0, 90)
    hold on
    
%     plot( Vx, -Vy, 'k' );

    
    plot( stax, -stay, 'ko','MarkerFaceColor','k','MarkerSize', 2 ); 
%     plot( Vx, -Vy, 'k' );  
    xt=[-700 -478 -256 -34 188 410 632 ]; yt= [-772 -550 -328 -106 116 338 560 782];
    xt=[-700 -478 -256 -34 188 410 632 ]; yt= [-772 -550 -328 -106 116 338 560 782];
    
%     xt=[-776 -556 -337 -117 102 322 541 761]; yt= [-550 -350 -150 50 250 450 650];
%     xt=[-776 -556 -337 -117 102 322 541 761]; yt= [-542 -319 -96 125 349 571];
    set(gca,'XTick',xt,'YTick',yt) 
    for gg=1:length(xt)
    [ytl,xtl] = project_xy( par, xt(gg)*ones(size(yt)), -yt, 'inverse' ); 
    xla(gg)=mean(xtl);
    end

    set(gca,'XTickLabel',roundn(xla,0.0),'YTickLabel',roundn(sort(ytl, 'descend'),0.0) )
    xlabel( 'Longitude ' );
    ylabel( 'Latitude ' );
    
     cs = max(abs([min(m_plot2(:)),max(m_plot2(:))]));
     caxis([-cs cs])
set(gcf,'units',get(gcf,'PaperUnits'),'Position',get(gcf,'PaperPosition'))
    
%     choice = input( 'Print?', 's' );
% 	if (~ isempty(choice) )
            print(['figures/wa_tomog_CRT' num2str(dh) '_' num2str(plotz(pz)) '.ps'],'-depsc','-r400');
% 	end
    pause(1)
end
