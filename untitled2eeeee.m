clear 
close all
clc
load 001_original_model_new

for h=1:60
 
hold on
valsflat = v_flattener(vm_f,h);
vm=valsflat;


  df = G*vm(:);
  
  dt_max(h)=max(df);
  dt_min(h)=min(df);
 
  serial=[1:length(df)]; hold on;
  ser_vr=[1:60];
%  format long;
%  
%  
%   subplot(3, 1, 1);
%   histogram(df,35,'FaceAlpha',0.15); hold on;
%  
  ddff = df(~isnan(df));
  dt_med(h)=median(ddff);
  dt_mean=mean(ddff);
  
  
%   subplot(3, 1, 2); hold on;
%   scatter(ser_vr,dt_max,1,'d'); hold on;
 
 
 
  vr(h)    = var_red( d, df(:) );
  fprintf( '\tVariance Reduction = %.3f%%\n', vr(h) );
 
 
end


hold on
subplot(3, 1, 1); hold on;
  scatter(ser_vr,dt_med,30,'d','b','filled'); hold on;
  ylim([-0.10 0.10]);
  grid on
  box on
   title('MEDIAN dt over 60 iterations'); ylabel('dt (seconds)'); xlabel('Observations Number');
hold on

subplot(3, 1, 2); hold on;
  scatter(ser_vr,dt_max,30,'d','b','filled'); hold on;
  grid on
  box on
  ylim([0.0 1.1]);
  
  
title('MAX dt over 60 iterations'); ylabel('dt (seconds)'); xlabel('Observations Number');
hold on

subplot(3, 1, 3); hold on;
  scatter(ser_vr,dt_min,30,'d','b','filled'); hold on;
    grid on
  box on
  ylim([-0.70 0.10]);
  title('MIN dt over 60 iterations'); ylabel('dt (seconds)'); xlabel('Observations Number');
 