clear 
close all
clc
load 001_original_model_new

for h=1:60 %60
 
hold on
valsflat = v_flattener(vm_f,h);
vm=valsflat;


  df = G*vm(:);
  dt_max=max(df);
  dt_min=min(df);
 
%   serial=[1:length(df)]; hold on;
%   ser_vr=[1:5];   %60
%  format long;
%  
%  
%   subplot(3, 1, 1);
%   histogram(df,35,'FaceAlpha',0.05); hold on;
%  
  ddff = df(~isnan(df));
  dt_med=median(ddff);
  dt_mean=mean(ddff);
  
%   
%   subplot(3, 1, 2); hold on;
%   scatter(serial,df,1,'d'); hold on;
%  
%  
 
  vr(h)    = var_red( d, df(:) );
  fprintf( '\tVariance Reduction = %.3f%%\n', vr(h) );
 
  
  
  it_fold=(['it_' num2str(h)]);
  mkdir(it_fold)
  save(['hfiles/df_' num2str(h) '.mat'],'df');
  save(['hfiles/vm_' num2str(h) '.mat'],'vm');
  upsample_mods(vm,par,1);
  cd (it_fold)
  it_fold
  !cp ../hfiles/plot* ./
  cd ..
end

 hold on
 subplot(3, 1, 3); hold on;
 scatter(ser_vr,vr,30,'d','b','filled'); 
 
 hold on
 grid on
 box on
 title('Variance Reduction'); ylabel('Variance Reduction [%]'); xlabel('iteration number');
 

%  hold on
%  subplot(3, 1, 2)
%  grid on
%  box on
%  title('dt over 60 iterations'); ylabel('dt (seconds)'); xlabel('Observations Number');
%  
%  hold on
%  subplot(3, 1, 1)
%  grid on
%  box on
%  title('dt histograms'); ylabel('Observations'); xlabel('dt (seconds)');
 
 
 %print(['60_ietrs_dfOUT_new'],'-dpdf','-fillpage','-r2000');