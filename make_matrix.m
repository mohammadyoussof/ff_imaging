function [G,data] = make_matrix( par, data )
disp('Youssof , this is now for making your matrix')
if ( nargin ~= 2 )
    error( 'incorrect number of input arguments' );
end

% input data and model space from run_tomo
stn  = data.stn;
ray  = data.ray;
modx = par.modx;
mody = par.mody;
modz = par.modz;
nx = length( modx );
ny = length( mody );
nz = length( modz );
nmod = nx*ny*nz;
%
node.bin1 = zeros(nmod,1);
node.bin2 = zeros(nmod,1);
node.bin3 = zeros(nmod,1);
node.bin4 = zeros(nmod,1);
node.core = zeros(nmod,1);

disconsZ = [ 0 20 35 77.5 120 165 210 260 310 360 410 460 510 ...
            560 610 660 710 760 1007.5 1502.5 2047 2492 2740 ]';
Vzd      = [ 5.8 6.5 8.04 8.045 8.05 8.175 8.3007 8.4822 8.665 ...
            8.8476 9.3601 9.5280 9.6962 9.864 10.032 10.7909 10.9222 ...
            11.0553 11.4704 12.1912 12.8524 13.3584 13.6498 ]';
zz       = 1:1:1250;
Vz   = interp1(disconsZ,Vzd,zz);
save Vz Vz
%M , i delete. %%disc = find(disconsZ<max(modz));%%
%disc = disconsZ<max(modz);
disc = find(disconsZ<max(modz));
tr_z = sort( [ modz' ; disconsZ(disc) ] );
z1   = tr_z(1:end-1);
z1   = unique(z1);
z2   = tr_z(2:end);
z2   = unique(z2);
v1   = interp1(disconsZ,Vzd,z1);
v2   = interp1(disconsZ,Vzd,z2);
par.vel.v1 = v1;
par.vel.v2 = v2;
par.vel.z1 = z1;
par.vel.z2 = z2;

nrays = ray.nrays;
data.ray.used = false( nrays, 1 );

indx = 1;
row = 1;
total_time = 0;
loop_time = 0;

s = [];     % sensitivity
si = [];    % row, event/ray
sj = [];    % column, non-zero for nodes w/sensitivity to ray
G = [];


h = waitbar(0,'');
waitbar_num = min( 100, round( nrays/100 ) );
    disp (' Hello Partial Derevatives ')
% main loop through all rays
for iray = 1:nrays          
    
	tic
	total_time = loop_time + total_time;
	mean_time = total_time / iray;
	is_update = should_update_waitbar( par, iray, waitbar_num, nrays );
	if ( is_update )
		update_waitbar( iray, nrays, mean_time, total_time, h );
    end
    %
    
	% ray trace
	[raylat, raylon] = go_ray_trace( par, ray, stn, iray);
	rayxyz1 = go_project_xy( par, raylat, raylon ); 
    
    mz_max = modz(end-1);
    if ray.pd(iray)>7.7
        nan1 = isnan(rayxyz1(:,1));
        keep1 = find(nan1==0);
        rmax  = length(keep1);
        zmax  = rayxyz1(rmax,3);
        rayxyz1 = rayxyz1(1:rmax,:);
        if zmax < modz(end-1)
            mz_max = zmax;
        end
    end
    zi = rayxyz1(:,3);
    zo = [modz(modz<=mz_max)]';
    xo = interp1(zi,rayxyz1(:,1),zo);
    yo = interp1(zi,rayxyz1(:,2),zo);
    rayxyz = [xo yo zo];
    
	data.ray.used(iray) = true;
    
    baz  = ray.baz(iray);
    p    = ray.p(iray);
    cf   = ray.cf(iray);
    chan = ray.chan(iray);
    
    [nodexyz,nodeindx,nodeval] = K_coeff( rayxyz, par, p, baz, cf, Vz, chan );

    if cf >= 0.15  % i changed this to 0.15 MY
        nodeindx2 = nodeindx(abs(nodeval)>0);
        if p<0.026
            node.core(nodeindx2) = node.core(nodeindx2)+ones(size(nodeindx2));
        elseif baz>=0 && baz<90
            node.bin1(nodeindx2) = node.bin1(nodeindx2)+ones(size(nodeindx2));
        elseif baz>=90 && baz<180
            node.bin2(nodeindx2) = node.bin2(nodeindx2)+ones(size(nodeindx2));
        elseif baz>=180 && baz<270
            node.bin3(nodeindx2) = node.bin3(nodeindx2)+ones(size(nodeindx2));
        elseif baz>=270 && baz<=360
            node.bin4(nodeindx2) = node.bin4(nodeindx2)+ones(size(nodeindx2));
        end
    end
          
	% dynamically keep sparse matrix buffers large enough for ~100 rays
	is_buffer_small = length(nodeval)*200 > length(s);
	if ( is_buffer_small )
		nbuff = length(nodeval)*200 - length(s);
		si = [si; zeros(nbuff,1)];
		sj = [sj; zeros(nbuff,1)];
		s = [s; zeros(nbuff,1)];
    end

	% index into sparse matrix buffers
	indx = indx(end):indx(end)+length(nodeval)-1;
	
	% fill sparse matrix buffers
	si(indx) = row;
	sj(indx) = nodeindx;
	s(indx) = nodeval;
    %save row row;  %MY added
	% if you filled buffers, grow G then reset
	if ( length(s) < indx(end)+length(nodeval) || iray == nrays )
		sindx = 1:indx(end);
		G = [G; sparse(si(sindx),sj(sindx),s(sindx),row,nmod)];
		%  i don't know but we will see :)  !!! %%%%
        save Gsee G
        save sisee si % row, event/ray
        save sjsee sj
        save rowsee row
        %G = G * W
        %  i don't know but we will see :)  !!! %%%%
        
        indx = 1;
		row = 1;
		notify( sprintf('Mohammad Youssof,,,,,Grew G at %d rays.',iray), par, 3 ) %I added MY
	else
		row = row + 1;
	end

	loop_time = toc;
    
    save raylat raylat
    save raylon raylon
    save rayxyz1 rayxyz1
    save rayxyznew rayxyz
    
 end
% 
%     save raylat_try raylat
%     save raylon_try raylon
%     save rayxyz1_try rayxyz1
%     save rayxyz_try rayxyz
    
data.node = node;
% end main loop through all rays

if ( par.verbose >= 2 )
	close( h );
end
msg = sprintf( '%.1f s (%.1f min) Elapsed Calculating Coeffs.', total_time, ...
	total_time/60 );
notify( msg, par, 2 );
if ( par.verbose >= 2 )
	info = whos( 'G' );
	fprintf( 'Unaugmented G is %g KB (%g MB, %g GB) with %d elements.\n', ...
		info.bytes/1e3, info.bytes/1e6, info.bytes/1e9, nnz(G) );
end
%     save raylat raylat
%     save raylon raylon
%     save rayxyz1 rayxyz1
%     save rayxyznew rayxyz
%%------ SUBFUNCTIONS --------%%%

function tf = should_update_waitbar( par, iray, waitbar_num, nrays )

tf = (par.verbose >= 2 && ~mod(iray,waitbar_num)) || iray == nrays || ...
	iray == 1 || iray == 10;


function update_waitbar( iray, nrays, mean_time, total_time, h )

pct = (iray-1)/nrays;
msg1 = [':)         Calculating Partial Dervitives: ' num2str(pct*100,'%.1f') '% done'];
msg1 = [msg1 ' (' int2str(iray) ' of ' int2str(nrays) ' rays)'];
est_time = (nrays-iray) * mean_time;
msg2 = [':)         Estimated Time of Completion: ' num2str(est_time/60,'%.1f') ' min'];
msg2 = [msg2 ' (' num2str(est_time/60/60,'%.2f') ' hours)'];
msg3 = [':)         Time Elapsed: ' num2str(total_time/60,'%.1f') ' min'];
waitbar( pct, h, {msg1 msg2 msg3} );

