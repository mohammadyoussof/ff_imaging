    %normal dataset%%%%%%%%%%%
    figure(1)
    subplot(6,1,1)
    fid = fopen('n3.dat', 'r');

    A = [textscan(fid, '%f %f %f %s %d %f %f %f %f %d'),1]; fclose(fid);
    ray.pd    = A{1};
    ray.p     = ray.pd .*(360/(2*pi*6371));
    ray.baz   = A{2};
    ray.d     = A{3}; 
    ray.sta   = A{4};
    ray.orid  = A{5};
    ray.cf    = A{6};
    ray.chan  = A{10};
    ray.lat   = A{7};
    ray.lon   = A{8};
    sta_ray   = ray.sta;
    ray.nrays = length(ray.p);
    
    histfit(ray.d,100)
    
    text(-1.0,150, {'SD' num2str(std(ray.d))});
     text(0.55,150, {'RMS' num2str(rms(ray.d))});
    set(gca,'Xlim',[-1.5 1.5],'Ylim',[0 500])
    title ( 'Complete Datatset')
    
    axes('Position',[.75 .85 .25 .085])
    box on
    rr=rose(ray.baz*pi/180,50);
    hline = findobj(gca, 'Type', 'line');
    set(hline, 'LineWidth', 1.5)
    set(gca,'view',[-90 90], 'YDir','reverse');
    x=get(rr,'Xdata');
    y = get(rr,'Ydata');
    gg=patch(x,y,'r');
    gg=patch(x,y,'b','EdgeColor','r','LineWidth',0.5);
    x=[];
%     
    
    %%REDUCED dataset%%%%
   subplot(6,1,2)
    fid = fopen('TDelay_Reduced_Evs_bin_various.txt', 'r');
    
   
    A = [textscan(fid, '%f %f %f %s %d %f %f %f %f %d'),1]; fclose(fid);
    ray_r.pd    = A{1};
    ray_r.p     = ray_r.pd .*(360/(2*pi*6371));
    ray_r.baz   = A{2};
    ray_r.d     = A{3};
    ray_r.sta   = A{4};
    ray_r.orid  = A{5};
    ray_r.cf    = A{6};
    ray_r.chan  = A{10};
    ray_r.lat   = A{7};
    ray_r.lon   = A{8};
    sta_ray_r   = ray_r.sta;
    ray_r.nrays = length(ray_r.p);
    histfit(ray_r.d,100)
    text(-1.0,100, {'SD' num2str(std(ray_r.d))});
     text(0.55,100, {'RMS' num2str(rms(ray_r.d))});
     set(gca,'Xlim',[-1.5 1.5],'Ylim',[0 500])
     
      title ( 'Reduced Datatset')
     
      hold on
   axes('Position',[.85 .75 .05 .085])
      %axes('Position',[.7 .45 .25 .15])
    box on
    rr=rose(ray_r.baz*pi/180,50);
    hline = findobj(gca, 'Type', 'line');
    set(hline, 'LineWidth', 1.0)
    set(gca,'view',[-90 90], 'YDir','reverse');
    x=get(rr,'Xdata');
    y = get(rr,'Ydata');
    gg=patch(x,y,'r');
    gg=patch(x,y,'g','EdgeColor','r','LineWidth',0.5);
    x=[];
    
  
    
    

    
    %%%%%% Weighted Datatset %%%%%%
    
   subplot(6,1,3)
    fid = fopen('n3.dat', 'r');
    A = [textscan(fid, '%f %f %f %s %d %f %f %f %f %d'),1]; fclose(fid);
    ray_w.pd    = A{1};
    ray_w.p     = ray_w.pd .*(360/(2*pi*6371));
    ray_w.baz   = A{2};
    ray_w.d     = A{3};
    
    for i = 1:length(ray.baz)
        if ray_w.baz(i)> 180 &&  ray_w.baz(i) <= 270
            ray_w.d(i)=ray_w.d(i)*0.5;
        end
    end
    
    ray_w.sta   = A{4};
    ray_w.orid  = A{5};
    ray_w.cf    = A{6};
    ray_w.chan  = A{10};
    ray_w.lat   = A{7};
    ray_w.lon   = A{8};
    sta_ray_w   = ray_w.sta;
    ray_w.nrays = length(ray_w.p);
    histfit(ray_w.d,100)
     %text(0.5,0.5,'\ite^{i\omega\tau} = cos(\omega\tau) + i sin(\omega\tau)')
     text(-1.0,200, {'SD' num2str(std(ray_w.d))});
     text(0.55,200, {'RMS' num2str(rms(ray_w.d))});
     title ( 'Weighted Datatset')
     set(gca,'Xlim',[-1.5 1.5],'Ylim',[0 500])
     
     hold on
      axes('Position',[.7 .15 .25 .15])
    box on
    rr=rose(ray_w.baz*pi/180,50);
    hline = findobj(gca, 'Type', 'line');
    set(hline, 'LineWidth', 1)
    set(gca,'view',[-90 90], 'YDir','reverse');
    x=get(rr,'Xdata');
    y = get(rr,'Ydata');
    gg=patch(x,y,'k');
    gg=patch(x,y,'y','EdgeColor','r','LineWidth',0.5);
    x=[];
    %set(gca,'Ylim',[0 400])
    
     %
        %text(-1.35,500, {'Mean' num2str(mean(ray_w.d))});
      
%         text(.55,500, {'Median' num2str(median(ray_w.d))});
%          
%          text(1.35,500, {'RMS' num2str(rms(ray_w.d))});
  

%%%%%%%%%%%%###
subplot(6,1,4), histfit(data.ray.corr1)
        xlabel( 'Time(seconds)' );
    ylabel( 'Observations' );
    title( sprintf('Crustal Corrections'));
    caxis( [-10.5  20.0] )
  
    set(gca,'Xlim',[-1.5 1.5],'Ylim',[0 500])
    %%%%%%%%%%%%%%%%%%%%
subplot(6,1,5), histfit(d)
        xlabel( 'Time(seconds)' );
        ylabel( 'observations' ); 
       title( sprintf('Relative Trveltimes Residuals (s) Before Inversion'));
       caxis( [-1.5  2.0] )
       set(gca,'Xlim',[-1.5 1.5],'Ylim',[0 500])
       %text(-1.35,500, {'Mean' num2str(mean(d))});
       % text(-.55,500, {'Median' num2str(median(d))});
       % text(0.55,500, {'SD' num2str(std(d))});
       % text(1.35,500, {'RMS' num2str(rms(d))});
       text(-1.0,200, {'SD' num2str(std(d))});
     text(0.55,200, {'RMS' num2str(rms(d))}); 
       %%%%%%%
        
        subplot(6,1,6), histfit(r_resids)
        xlabel( 'Time(seconds)' );
    ylabel( 'Observations' );
    title( sprintf('Relative Trveltimes Residuals (s) After Inversion'));
      caxis( [ -1.5  4.0] )
     set(gca,'Xlim',[-1.5 1.5],'Ylim',[0 500])
%       text(-1.35,500, {'Mean' num2str(mean(r_resids))});
%         text(-.55,500, {'Median' num2str(median(r_resids))});
%         text(0.55,500, {'SD' num2str(std(r_resids))});
%         text(1.35,500, {'RMS' num2str(rms(r_resids))});

        text(-1.0,200, {'SD' num2str(std(r_resids))});
     text(0.55,200, {'RMS' num2str(rms(r_resids))}); 
%%%%%%%%%print(['Histograms_measurements_NEW.pdf'],'-dpdf','-r1000');
    
   
   
   