function plot_s_static(par, data, s_static)

load tomo_cmap;
colormap( tomo_cmap );

colormap('default');

stn = data.stn;
bbox = [min([stn.lon,stn.lat]); max([stn.lon,stn.lat])];
V = shaperead( 'unep_reported_malaria_cases_-_total_number_per_100000_population_world_1990-2003.shp', 'BoundingBox', bbox);
V1 =shaperead('ANG-15_boundaries.shp','BoundingBox', bbox);
for i = 1:length(V)
	[x,y] = project_xy( par, V(i).Y, V(i).X );
	plot( x, y );
end
hold on

for i = 1:length(V1)
	[x1,y1] = project_xy( par, V1(i).Y, V1(i).X );
	plot( x1, y1 );
end
hold on
h = plot( stn.x, stn.y, 'ko' );
set( h, 'markerfacecolor', 'r' );

min_x = min(data.stn.stax)-100;
max_x = max(data.stn.stax)+100;
min_y = min(data.stn.stay)-100;
max_y = max(data.stn.stay)+100;




%%%clf
scatter( data.stn.x, data.stn.y, 400, s_static, '.' ); 
hold on

colorbar
caxis( [min(s_static) max(s_static)] );
axis( [min_x max_x min_y max_y] );
daspect([1 1 1])



xt=[]; yt=[];
set(gca,'XTick',xt,'YTick',yt) 

title('Station static terms (s)');
hold on
grid on 
box on

print(['figures/Stn_Static_W3.pdf'],'-dpdf','-r300');

pause(1)