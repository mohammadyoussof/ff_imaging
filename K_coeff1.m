function [nodexyz,nodeindx,nodeval] = K_coeff1( rayxyz, par, p, baz, cf, Vz,chan)
disp('Calculations radius from nearest node where sens~=0 for ray center freq ')
modx = par.modx;
mody = par.mody;
modz = par.modz;
nx = length( modx );
ny = length( mody );
nz = length( modz );
dh = 10;            % estimate for node spacing

% find node nearest to each pierce_pt
i = round(interp1(mody,1:length(mody),rayxyz(:,2),'linear'));
j = round(interp1(modx,1:length(modx),rayxyz(:,1),'linear'));
k = round(interp1(modz,1:length(modz),rayxyz(:,3),'linear'));

% generously calc max radius from nearest node where sens~=0 for ray center freq.
r_kmax = 80*(cf^(-0.35));
r_nodes = ceil((2*r_kmax)/dh);

nseg = size(i,1);
nodexyz = zeros( nseg*(r_nodes^2), 3 );
nodeindx = zeros( nseg*(r_nodes^2), 1 );
nodeval = zeros( nseg*(r_nodes^2), 1 );

cnt = 1;
for iseg = 1:nseg
   if (cf > 1)  %
        ii = [ i(iseg) i(iseg)+1 i(iseg)-1 ];
        ii = ii( ii<length(mody) );
        ii = ii( ii>0 );
        jj = [ j(iseg) j(iseg)+1 j(iseg)-1 ];
        jj = jj( jj<length(modx) );
        jj = jj( jj>0 );
    elseif (cf <= 1 && cf > 0.3)
        ii = [ i(iseg) i(iseg)+1 i(iseg)-1 i(iseg)+2 i(iseg)-2];
        ii = ii( ii<length(mody) );
        ii = ii( ii>0 );
        jj = [ j(iseg) j(iseg)+1 j(iseg)-1 j(iseg)+2 j(iseg)-2];
        jj = jj( jj<length(modx) );
        jj = jj( jj>0 );
    elseif (cf <= 0.3 && cf >= 0.2)
        ii = [ i(iseg) i(iseg)+1 i(iseg)-1 i(iseg)+2 i(iseg)-2 i(iseg)+3 i(iseg)-3];
        ii = ii( ii<length(mody) );
        ii = ii( ii>0 );
        jj = [ j(iseg) j(iseg)+1 j(iseg)-1 j(iseg)+2 j(iseg)-2 j(iseg)+3 j(iseg)-3];
        jj = jj( jj<length(modx) );
        jj = jj( jj>0 );
    elseif cf < 0.2  
        ii = [ i(iseg) i(iseg)+1 i(iseg)-1 i(iseg)+2 i(iseg)-2 i(iseg)+3 i(iseg)-3 ...
            i(iseg)+4 i(iseg)-4];
        ii = ii( ii<length(mody) );
        ii = ii( ii>0 );
        jj = [ j(iseg) j(iseg)+1 j(iseg)-1 j(iseg)+2 j(iseg)-2 j(iseg)+3 j(iseg)-3 ...
            j(iseg)+4 j(iseg)-4];
        jj = jj( jj<length(modx) );
        jj = jj( jj>0 );
   end   
   if (length(jj)*length(ii))>=1
       nodei = zeros(length(ii)*length(jj),1);
       nodej = zeros(length(ii)*length(jj),1);
       nn=1;
       for qi = 1:length(ii)
           for qj = 1:length(jj)
                nodei(nn) = abs(round(ii(qi)));
                %nodei 
                nodej(nn) =  abs(round(jj(qj)));
                %nodej
                nn = nn+1;
           end
       end
       nodei = nodei(nodei>0);
       nodej = nodej(nodej>0);
       nodek = k(iseg)*ones(size(nodei));
       qq = 1:length(ii)*length(jj);
       nodey = mody(nodei(qq));
       nodex = modx(nodej(qq));
       a   = [modx(jj(1)) mody(ii(1)) ];
       ppt = rayxyz(iseg,:);
       npy = nodey-ppt(2);
       npx = nodex-ppt(1);
       rp  = ( npx.^2 + npy.^2 ).^(0.5);
       theta = atan2(npx(:),npy(:));
       for qq = 1:length(rp)
       nodeindx(cnt+qq-1) = fast_sub2ind3d( [ny nx nz], nodei(qq), nodej(qq), nodek(qq) );
       nodexyz(cnt+qq-1,:)  = [modx(nodej(qq)), mody(nodei(qq)), modz(nodek(qq))];
       end
       nodeval(cnt:cnt+length(rp)-1,:)  = Kval(par,a,rp,theta,k(iseg),p,baz,cf,Vz,chan);
   end
   cnt = cnt+length(nodei);
end

pp = find(abs(nodeval)>0);
nodeindx = nodeindx(pp);
nodexyz = [ nodexyz(pp,1) nodexyz(pp,2) nodexyz(pp,3) ];
nodeval = nodeval(pp);
save nodexyz nodexyz
save nodeval nodeval
save nodei nodei
save nodej nodej
save nodek nodek

