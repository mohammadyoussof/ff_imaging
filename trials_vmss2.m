clear 
close all
clc

load 001_original_model_new
%vm_f = vm;

for h = 1:50
    vm_f(h) = vm(h);
valsflat(h) = v_flattener(vm_f(h),h);
vm(h)=valsflat(h);

df(h(:)) = G(h(:))*vm(h(:));
vr(h)    = var_red( d, df(:) );
fprintf( '\tVariance Reduction = %.3f%%\n', vr(h) );

  
%pName=h;

%hfold(1) = mkdir(['ok_' num2str(h)]);
hfold=(['iter_' num2str(h)]);

%foldername = sprintf('hfold%02',hfold)
mkdir(hfold)



%FolderDestination=strcat(num2str(hfold))
%cd hfold
save(['hfiles/df_' num2str(h) '.mat'],'df');
save(['hfiles/vm_' num2str(h) '.mat'],'vm');

upsample_mods(vm,par,1);

 cd (hfold)
 
 hfold
 
 !cp ../hfiles/plot* ./
%!cp plot_pts.mat .
%!cp hfiles/plot_pts.mat plot_pts.mat

 cd ..

%!cp hfiles/plot_pts.mat hfold/plot_pts.mat

%cd 
%!cp hfiles/plot_pts.mat hfold/plot_pts.mat

%!cp plot_pts hfiles/hfold/plot_pts


%vr (h)   = variance_reduction( d, df(h(:)) )
%dc(h(:)) = calc_dc(s_static,e_static,df(h(:)),ray,stn); 
end


%cd hfiles


% .         save var_red_values vr

%vm_out001=vm;
%df_out001=df;


%noise = randn(size(df))*2.5; %gaussian noise , sd randn gaussain  
%df = df +noise;

% 
% dc = calc_dc(s_static,e_static,df(:),ray,stn); 
% vr    = variance_reduction( d, dc(:) );
% fprintf( '\tVariance Reduction = %.3f%%\n', vr );

%  for j=1:6
% XX=j^2;
% save(['XX_' num2str(j) '.mat'],'XX')
% end