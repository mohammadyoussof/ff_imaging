clc
clear 
close all
load data;
%load block_mod_invention2.mat
load block_mod_invention1.mat
d1=df;
d2=dR;

%s_static1  = m(nmodel+1:end_s_stat);
s_static1  = m(end_s_stat-79:nmodel);
%e_static1  = m(end_s_stat+1:end);
e_static1  = m(end-279:end);
dc = calc_dc(s_static1,e_static1,d1(:),ray,stn); 
%vr    = variance_reduction( d1, dc(:) );

resid1 = norm( dc(:) - d1 );



stn.resid1 = zeros(length(stn.num),1);
for ii = 1:length(stn.num)
    indx2 = find(ray.sta_num == stn.num(ii));
    stn.resid1(ii) = sum( dc(indx2)-d1(indx2) )/length(indx2); %# TRIALS
    %stn.resid(ii) = sum( (dc(indx2)-d(indx2)) )/length(indx2);
end   


figure(1)
 s_residuals1=stn.resid1;
 plot_s_resids(par, data, s_residuals1); hold on; set(gca,'clim', [(-3e-04) (3e-04)]); cs = max(abs([(-3e-04), (3e-04)]));
 
 figure(2);
  
histfit(s_residuals1(:), 10)
 xlim([-4e-4 3e-4])
 ylim([0 17])
title('Residuals Terms')
xlabel('seconds')
print(['figures_true_ref21/histo_resd1.pdf'],'-dpdf','-fillpage','-r1000');

figure(30);
  
histogram(d2(:))
 %xlim([-4e-4 3e-4])
 %ylim([0 17])
title('d2 dR Terms')
xlabel('seconds')
