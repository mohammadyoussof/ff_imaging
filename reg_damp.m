function G_damp = reg_damp( data, par)

nstas = length(unique(data.ray.sta_num));   nevts = length(unique(data.ray.orid));
nx = length( par.modx );
ny = length( par.mody );
nz = length( par.modz );
nmod = nx*ny*nz;                          
nmat = nmod+nstas+nevts;
%hit_q = data.node.hit_q;

G_d1 = spalloc(nmod,nmat,nmod+1);
G_d2 = spalloc(nstas,nmat,nstas+1);
G_d3 = spalloc(nevts,nmat,nevts+1);

tic
h = waitbar( 0, 'Building norm damping matrix...' );
for kk = 1:nz
    waitbar( (kk-1)/nz, h );	
    G_d = spalloc(nx*ny,nmat,nx*ny+1);
    wt1  = 1;%-0*par.modz(kk); 

    for jj = 1:nx
        for ii = 1:ny
           n_indx = fast_sub2ind3d( [ny nx nz], ii, jj, kk );
           if ( kk==1 )
                wt2 = 0.95;
           elseif kk == nz
                wt2 = 0.95;
           else
               wt2 = 1;
           end

           wt = wt1*wt2;
           G_d(ii+ny*(jj-1), :) = sparse(1,n_indx,wt,1,nmat); 
        end
    end
    G_d1 = [G_d1; G_d];
end


for indx = 1:nstas  
   wt = par.damp_stn;
   ind = indx+nmod;
   G_d2(indx,:) = sparse(1,ind,wt,1,nmat);
end


for indx = 1:nevts  
   wt = par.damp_evt;
   ind = indx+nmod+nstas;
   G_d3(indx,:) = sparse(1,ind,wt,1,nmat);
end
close(h);

toc
G_damp = [ G_d1; G_d2; G_d3 ];